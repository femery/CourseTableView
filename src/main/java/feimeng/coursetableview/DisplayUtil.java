package feimeng.coursetableview;

import android.content.Context;

/**
 * 显示大小工具类
 * Created by penfi on 2015/11/14.
 */
public class DisplayUtil {


    public static int dp2px(Context context, float dp) {

        return (int) (dp * getScale(context) + 0.5f);
    }

    public static float getScale(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static float getScaledDensity(Context context) {
        return context.getResources().getDisplayMetrics().scaledDensity;
    }

    /**
     * sp转成px
     *
     * @param sp sp
     */
    public static float sp2px(Context context, float sp) {
        return sp * getScaledDensity(context);
    }
}
