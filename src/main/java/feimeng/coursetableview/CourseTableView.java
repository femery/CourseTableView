package feimeng.coursetableview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;


/**
 * 课程表 控件
 * Created by penfi on 2015/11/12.
 */
public class CourseTableView extends View {
    private int mBorderColor;// 边框颜色
    private Paint mPaint;
    private Table mTable;// 表格
    private Section mSection;// 节

    private String[] mDayTitle;// 头部内容
    private String[] mSectionTitle;// 节标题

    private int mSectionNumOfDay;// 一天的节数
    private int mDayNumOfWeek;// 一周天数
    private boolean mDrawBorder;// 是否绘制边框

    private CourseTableManager manager;// 管理器


    private List<SimpleSection> mSimpleSections;

    public CourseTableView(Context context) {
        this(context, null);
    }

    public CourseTableView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CourseTableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 获取自定义属性
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CourseTableView);
        mBorderColor = ta.getColor(R.styleable.CourseTableView_coursetable_borderColor, Color.parseColor("#00bbff"));
        mDrawBorder = ta.getBoolean(R.styleable.CourseTableView_coursetable_drawBorder, true);
        Bitmap background = BitmapFactory.decodeResource(getResources(), ta.getResourceId(R.styleable.CourseTableView_coursetable_background, 0));
        ta.recycle();// 释放资源

        mTable = new Table();// 表格对象

        mTable.setContentStyle((int) DisplayUtil.sp2px(getContext(), 12), Color.WHITE);
        mTable.setTitleStyle((int) DisplayUtil.sp2px(getContext(), 15), Color.BLACK);
        mTable.setBackground(background);
        mSection = new Section();// 节对象

        mPaint = new Paint();// 画笔
        mPaint.setAntiAlias(true);// 抗锯齿功能

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else {
            width = widthSize;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else {
            height = heightSize;
        }
        setMeasuredDimension(width, height);

//        Log.i("feimeng_table", "width:" + width + " height:" + height);
        mDayNumOfWeek = mDayTitle.length;// 可以通过数组长度获得数目
        mSectionNumOfDay = mSectionTitle.length;

        // 初始化边框
        mTable.setBound(getMeasuredWidth(), getMeasuredHeight(), getPaddingLeft(),
                getPaddingTop(), getPaddingRight(), getPaddingBottom());
        int size = DisplayUtil.dp2px(getContext(), 30);
        mTable.setCornerSize(size, size);

        // 初始化节 宽高
        mSection.setSize((mTable.getWidth() - mTable.getCornerWidth()) / mDayNumOfWeek,
                (mTable.getHeight() - mTable.getCornerHeight()) / mSectionNumOfDay,
                DisplayUtil.dp2px(getContext(), 3), DisplayUtil.dp2px(getContext(), 6));

        mTable.setSection(mSection);

        mTable.setDayAndSectionNum(mDayNumOfWeek, mSectionNumOfDay);
        mTable.setDrawborder(mDrawBorder);
    }

    @Override
    protected void onDraw(Canvas canvas) {

/*
        Paint.Style.FILL    :填充内部
        Paint.Style.FILL_AND_STROKE  ：填充内部和描边
        Paint.Style.STROKE  ：仅描边*/
//        paint.setShadowLayer(10, 15, 15, Color.GREEN);//设置阴影

//        drawBorder(canvas);
        // 设置画笔
        mPaint.setColor(mBorderColor);// 设置画笔颜色
        mPaint.setStrokeWidth(2);// 设置画笔宽度
        mPaint.setStyle(Paint.Style.STROKE);// 设置填充样式 仅描边
        try {
            // 绘制背景
            mTable.drawBackground(canvas, mPaint);
            // 绘制边框
            mTable.drawBorder(canvas, mPaint);
            // 绘制标题
            mTable.drawTitle(canvas, mPaint, mDayTitle, mSectionTitle);
            // 绘制内容

        } catch (CourseTableViewException e) {
            e.printStackTrace();
        }
        mTable.drawContent(canvas, mPaint, mSimpleSections);
    }

    public int getmBorderColor() {
        return mBorderColor;
    }

    public void setmBorderColor(int mBorderColor) {
        this.mBorderColor = mBorderColor;
    }

    public String[] getmSectionTitle() {
        return mSectionTitle;
    }

    public void setmSectionTitle(String[] mSectionTitle) {
        this.mSectionTitle = mSectionTitle;
    }

    public String[] getmDayTitle() {
        return mDayTitle;
    }

    public void setmDayTitle(String[] mDayTitle) {
        this.mDayTitle = mDayTitle;
    }

    public List<SimpleSection> getmSimpleSections() {
        return mSimpleSections;
    }

//    public void setmSimpleSections(List<SimpleSection> simpleSections) {
//        this.mSimpleSections = simpleSections;
//    }
//
//    public void updateSimpleSections(List<SimpleSection> simpleSections) {
//        this.mSimpleSections = simpleSections;
//        invalidate();
//    }

    public void setSectionBgColors(int[] colors) {
        mSection.setBackgroundColors(colors);
    }

    public void setManager(CourseTableManager manager) {
        this.manager = manager;
        this.mSimpleSections = manager.getDatasByWeek();
    }

    /**
     * 更新
     */
    public void update() {
        mSimpleSections = manager.getDatasByWeek();
        invalidate();
    }
}
