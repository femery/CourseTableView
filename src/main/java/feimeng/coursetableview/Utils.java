package feimeng.coursetableview;

/**
 * 工具类
 * Created by feimeng on 2016/2/23.
 */
public class Utils {
    /**
     * 判断两个区间是否重复
     *
     * @param firstStart  第一个区间开始
     * @param firstEnd    第一个区结束
     * @param secondStart 第二个区间开始
     * @param secondEnd   第二个区间结束
     * @return
     */
    public static boolean isRepeat(int firstStart, int firstEnd, int secondStart, int secondEnd) {
        // 两种情况：
        // 1.第一个区间结束小于第二个区间开始
        // 2.第二个区间结束小于第一个区间开始
        if (firstEnd < secondStart || secondEnd < firstStart) {
            return false;// 无重复
        }
        return true;// 有重复
    }
}
