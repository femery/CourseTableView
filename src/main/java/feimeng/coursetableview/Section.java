package feimeng.coursetableview;

import android.graphics.Color;

/**
 * 节类
 * Created by penfi on 2015/11/14.
 */
public class Section {
    private int sectionWidth;// 节宽度
    private int sectionHeight;// 节高度
    private int[] sectionColors = new int[]{
            Color.parseColor("#5FC0E6"),
            Color.parseColor("#95D262"),
            Color.parseColor("#EB9FA1"),
            Color.parseColor("#F1C973"),
            Color.parseColor("#73C0AD")
    };// 节背景颜色
    private int sectionMargin = 5;// 节外边距 默认为5
    private int round = 10;// 圆角半径 默认为10

    public void setBackgroundColors(int[] sectionColors) {
        this.sectionColors = sectionColors;
    }

    public int getSectionWidth() {
        return sectionWidth;
    }

    public int getSectionHeight() {
        return sectionHeight;
    }

    public int getSectionColor(int poi) {
        return sectionColors[poi];
    }

    public int getSectionMargin() {
        return sectionMargin;
    }

    public int getRound() {
        return round;
    }

    public void setSize(int sectionWidth, int sectionHeight, int sectionMargin, int round) {
        this.sectionWidth = sectionWidth;
        this.sectionHeight = sectionHeight;
        this.sectionMargin = sectionMargin;
        this.round = round;
    }
}
