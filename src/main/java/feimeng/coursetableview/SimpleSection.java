package feimeng.coursetableview;

/**
 * Created by penfi on 2015/11/14.
 */
public class SimpleSection {
    int day;// 天
    int startSection;// 开始节
    int endSection;// 结束节
    int weekStart;// 开始周次
    int weekEnd;// 结束周次

    int startX, StartY;
    int width, height;

    String content;// 内容

    public SimpleSection(int day, int weekStart, int weekEnd, int startSection, int endSection, String content) {
        this.day = day;
        this.weekStart = weekStart;
        this.weekEnd = weekEnd;
        this.startSection = startSection;
        this.endSection = endSection;
        this.content = content;
    }

    /**
     * @return 1~5
     */
    public int getDay() {
        return day;
    }

    public int getWeekEnd() {
        return weekEnd;
    }

    public int getWeekStart() {
        return weekStart;
    }

    public int getStartSection() {
        return startSection;
    }

    public int getEndSection() {
        return endSection;
    }

    public String getContent() {
        return content;
    }
}
