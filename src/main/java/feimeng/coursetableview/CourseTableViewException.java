package feimeng.coursetableview;

/**
 * 课程表控件 异常类
 * Created by penfi on 2015/11/14.
 */
public class CourseTableViewException extends Exception {

    public CourseTableViewException() {
    }

    public CourseTableViewException(String message) {
        super(message);
    }
}
