package feimeng.coursetableview;


import java.util.ArrayList;
import java.util.List;

/**
 * 课程表数据管理器
 * Created by feimeng on 2016/2/22.
 */
public class CourseTableManager {
    private List<SimpleSection> allDatas;// 所有课程数据
    private int week;// 当前周次

    /**
     * @param allDatas 所有课程数据
     * @param week     当前周次
     */
    public CourseTableManager(List<SimpleSection> allDatas, int week) {
        this.allDatas = allDatas;
        this.week = week;
    }

    /**
     * 设置所有课程数据
     *
     * @param allDatas 所有课程数据
     */
    public void setDatas(List<SimpleSection> allDatas) {
        this.allDatas = allDatas;
    }

    /**
     * 设置当前周次
     * @param week 当前周次
     */
    public void setWeek(int week){
        this.week = week;
    }
    /**
     * 获取本周的课程表
     * @return 课程数据
     */
    public List<SimpleSection> getDatasByWeek() {
        List<SimpleSection> datas = new ArrayList<>();
        for (SimpleSection data : allDatas) {
            if (data.getWeekStart() <= week && week <= data.getWeekEnd()) {
                datas.add(data);
            }
        }
        return datas;
    }
}
