package feimeng.coursetableview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import java.util.List;

/**
 * 课程表 表格类
 * Created by penfi on 2015/11/14.
 */

public class Table {
    private int left;
    private int top;
    private int width, height;
    private int cornerWidth, cornerHeight;

    private Section section;
    private int dayNumOfWeek, sectionNumOfDay;

    private int titleSize = 25;// 标题字体大小
    private int titleColor = Color.BLACK;// 标题字体颜色
    private int contentSize = 25;// 内容字体大小 px
    private int contentColor = Color.WHITE;// 内容字体颜色 白色
    private boolean drawborder;// 是否绘制边框
    private Bitmap background;// 背景图片

    public Table() {

    }

    public void setBound(int measuredWidth, int measuredHeight, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        this.left = paddingLeft;
        this.top = paddingTop;

        int right = measuredWidth - paddingRight;
        int bottom = measuredHeight - paddingBottom;

        this.width = right - this.left;
        this.height = bottom - this.top;
    }

    public void setCornerSize(int cornerWidth, int cornerHeight) {
        this.cornerWidth = cornerWidth;
        this.cornerHeight = cornerHeight;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public void setDayAndSectionNum(int dayNumOfWeek, int sectionNumOfDay) {
        this.dayNumOfWeek = dayNumOfWeek;
        this.sectionNumOfDay = sectionNumOfDay;
    }

    public void setContentStyle(int contentSize, int contentColor) {
        this.contentSize = contentSize;
        this.contentColor = contentColor;
    }

    public void setTitleStyle(int titleSize, int titleColor) {
        this.titleSize = titleSize;
        this.titleColor = titleColor;
    }

    public void setDrawborder(boolean drawborder) {
        this.drawborder = drawborder;
    }

    public void setBackground(Bitmap background) {
        this.background = background;
    }

    /**
     * 绘制背景
     *
     * @param canvas
     * @param paint
     */
    public void drawBackground(Canvas canvas, Paint paint) throws CourseTableViewException {
        // 前提条件判断
        if (background == null) {
            throw new CourseTableViewException("无背景图片");
        }
        RectF rect = new RectF(0, 0, width, height);
        canvas.drawBitmap(background, null, rect, null);
    }

    /**
     * 绘制边框
     *
     * @param canvas
     * @param paint
     * @throws CourseTableViewException
     */
    public void drawBorder(Canvas canvas, Paint paint) throws CourseTableViewException {
        // 前提条件判断
        if (section == null)
            throw new CourseTableViewException("节还没有定义");
        if (dayNumOfWeek == 0 || sectionNumOfDay == 0)
            throw new CourseTableViewException("天数或节数没有定义");

        //绘制边框
        canvas.save();
        canvas.translate(left, top);

        if (drawborder) {
            Rect rect = new Rect(0, 0, width, height);
            canvas.drawRect(rect, paint);
        }

        // 画竖线
        // 左边头部竖线
        canvas.drawLine(cornerWidth, 0,
                cornerWidth, height, paint);

        for (int i = 1; i < dayNumOfWeek; i++) {
            canvas.drawLine(
                    cornerWidth + section.getSectionWidth() * i, 0,
                    cornerWidth + section.getSectionWidth() * i, cornerHeight,
                    paint);
        }
        // 画横线
        // 头部的横线
        canvas.drawLine(0, cornerHeight,
                width, cornerHeight, paint);

        for (int i = 1; i < sectionNumOfDay; i++) {
            canvas.drawLine(
                    0, cornerHeight + section.getSectionHeight() * i,
                    cornerWidth, cornerHeight + section.getSectionHeight() * i,
                    paint);
        }

        canvas.restore();
    }

    public void drawTitle(Canvas canvas, Paint paint, String[] titles, String[] sectionTitles) throws CourseTableViewException {
        // 前提条件判断
        if (section == null)
            throw new CourseTableViewException("节还没有定义");
        if (dayNumOfWeek == 0 || sectionNumOfDay == 0)
            throw new CourseTableViewException("天数或节数没有定义");

        canvas.save();
        canvas.translate(left, top);

        Rect bound = new Rect();
        paint.setStyle(Paint.Style.FILL);// 填充内部
        paint.setTextSize(titleSize);
        paint.setColor(titleColor);

        for (int i = 0; i < dayNumOfWeek; i++) {
            paint.getTextBounds(titles[i], 0, titles[i].length(), bound);
            canvas.drawText(titles[i],
                    cornerWidth + section.getSectionWidth() * i +
                            section.getSectionWidth() / 2 - bound.width() / 2,
                    cornerHeight / 2 + bound.height() / 2, paint);
        }

        for (int i = 0; i < sectionNumOfDay; i++) {
            paint.getTextBounds(sectionTitles[i], 0, sectionTitles[i].length(), bound);
            canvas.drawText(sectionTitles[i],
                    cornerWidth / 2 - bound.width() / 2,
                    cornerWidth + section.getSectionHeight() / 2 + section.getSectionHeight() * i +
                            bound.height() / 2, paint);
        }
        canvas.restore();
    }

    public void drawContent(Canvas canvas, Paint paint, List<SimpleSection> sections) {
        int save = canvas.save();
        canvas.translate(left, top);

        for (SimpleSection simpleSection : sections) {
            drawSection(canvas, paint, simpleSection);
        }

        canvas.restoreToCount(save);
    }

    private void drawSection(Canvas canvas, Paint paint, SimpleSection simpleSection) {
        int startX = (simpleSection.getDay() - 1) * section.getSectionWidth()
                + section.getSectionMargin();
        int StartY = (simpleSection.getStartSection() - 1) * section.getSectionHeight()
                + section.getSectionMargin();

        int sectionWidth = section.getSectionWidth() - section.getSectionMargin() * 2;
        int sectionHeight = (simpleSection.getEndSection() - simpleSection.getStartSection() + 1) *
                section.getSectionHeight() - section.getSectionMargin() * 2;

        // 绘制背景
        int dx = cornerWidth + startX;
        int dy = cornerHeight + StartY;

        int color;

        switch (simpleSection.getStartSection()) {
            case 1:
                color = 0;
                break;
            case 3:
                color = 1;
                break;
            case 5:
                color = 2;
                break;
            case 7:
                color = 3;
                break;
            default:
                color = 4;
                break;
        }

        paint.setColor(section.getSectionColor(color));
        paint.setStyle(Paint.Style.FILL);
        RectF sectionRect = new RectF(dx, dy, dx + sectionWidth, dy + sectionHeight);
        canvas.drawRoundRect(sectionRect, section.getRound(), section.getRound(), paint);

        // 绘制文本信息
        TextPaint textPaint = new TextPaint();
        textPaint.setTextSize(contentSize);
        textPaint.setColor(contentColor);

        Rect bound = new Rect();

        StaticLayout layout = new StaticLayout(simpleSection.getContent(), textPaint, sectionWidth, Layout.Alignment.ALIGN_CENTER, 1.0F, 0.0F, true);
        textPaint.getTextBounds(simpleSection.getContent(), 0, 1, bound);
        int h = bound.height() * layout.getLineCount();

        dy = dy + sectionHeight / 2 - 2 * section.getSectionMargin() - h / 2;

        canvas.save();//锁画布(为了保存之前的画布状态)
        canvas.translate(dx, dy);//把当前画布的原点移到(dx,dy),后面的操作都以(dx,dy)作为参照点，默认原点为(0,0)

        layout.draw(canvas);

        canvas.restore();//把当前画布返回（调整）到上一个save()状态之前
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCornerWidth() {
        return cornerWidth;
    }

    public int getCornerHeight() {
        return cornerHeight;
    }
}
